import unittest
import ce.h_atom as ce_h
import ce.grid as grid
import numpy as np


class TestHAtomRadial(unittest.TestCase):
    def test_grid_solution_consistency(self):
        a = 0
        b = 1
        n = 100
        r = np.linspace(a, b, n + 1)

        g = grid.Grid1D(a, b, n)
        radial = ce_h.Radial()
        v = radial.compute(g)

        self.assertEqual(r.shape[0], v.shape[0])
