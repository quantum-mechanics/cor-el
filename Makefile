format:
	@black ce/ tests/

run:
	@python -m ce.main

debug:
	@python -m pdb ce/main.py
