import math
import numpy as np


def norm_log(dx, r: np.ndarray, wf: np.ndarray) -> float:
    return math.sqrt(np.sum(wf**2 * r**2 * dx))


def normalize(dx, wf: np.ndarray) -> np.ndarray:
    return wf / math.sqrt(np.sum(wf**2 * dx))
