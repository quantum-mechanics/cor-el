import math
import numpy as np
import scipy.special as spec
import ce.grid as grid

from ce.wave_function import WaveFunction
from ce.unit_provider import UnitProvider
from ce.enums import MetricSystem


def energy(n: int, up: UnitProvider):
    return -(up.m_e / 2 / up.hbar**2 * (up.e**2 * up.k_e) ** 2) / n**2


def hydrogen_radial(
    q_num: tuple[int, int],
    boundary_factors: tuple[float, float],
    metric: MetricSystem = MetricSystem.AU,
) -> WaveFunction:
    up = UnitProvider(metric)
    a_0 = up.a_0

    n, l = q_num
    E = energy(n, up)

    a = math.log(a_0 * boundary_factors[0])
    b = math.log(a_0 * boundary_factors[1])
    nodes = 500
    g = grid.Grid1DLog(a, b, nodes)
    R_n_l = Radial(up=up)

    R_n_l.n = n
    R_n_l.l = l
    wf = R_n_l.compute(g)
    return WaveFunction(wf, g.r, E)


class Radial:
    def __init__(self, n: int = 1, l: int = 0, up: UnitProvider = UnitProvider()):
        assert n >= 1, "Quantum number n must be an integer >= 1"
        assert n - l >= 1, f"{n=}, {l=} are bad radial coefficients pair"

        self.n = n
        self.l = l
        self.up = up

    def compute(self, g: grid.Grid1D):
        r = g.r
        l = self.l
        n = self.n
        a = self.up.a_0

        coef = math.sqrt(
            ((2 / n / a) ** 3)
            * math.factorial(n - l - 1)
            / (2 * n * math.factorial(n + l))
        )
        rho = 2 * r / n / a
        res = (
            coef
            * np.exp(-rho / 2)
            * np.power(rho, l)
            * spec.assoc_laguerre(rho, n - l - 1, 2 * l + 1)
        )

        return res


class RadialTransformed(Radial):
    """
    Corresponds to the transformed function, used to
    generate equation of the same form as Schrodinger Equation
    by transforming the coordinates:

    r(x) -> e^x,

    and thereby obtaining the equation in terms of x. That equation
    is further transformed to get rid of terms that do not appear in
    radial Schrodinger Equation like first derivatives:

    u(x) -> sqrt(r(x)) * v(x),

    this is done to be able to apply Numerov trick.

    """

    def compute(self, g: grid.Grid1DLog):
        assert isinstance(
            g, grid.Grid1DLog
        ), f"{g=} is unsuitable. Only {grid.Grid1DLog.__name__} is accepted."

        r = g.r
        res = super().compute(g)
        return res / np.sqrt(r)
