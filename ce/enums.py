from enum import Enum, auto


class MetricSystem(Enum):
    SI = auto()
    AU = auto()
