from ce.enums import MetricSystem
import scipy.constants as const


class UnitProvider:
    def __init__(self, metric_system: MetricSystem = MetricSystem.AU):
        self.ms = metric_system

    def set_metric_system(self, metric_system: MetricSystem):
        self.ms = metric_system

    @property
    def e(self):
        return 1.0 if self.ms == MetricSystem.AU else const.e

    @property
    def m_e(self):
        return 1.0 if self.ms == MetricSystem.AU else const.m_e

    @property
    def hbar(self):
        return 1.0 if self.ms == MetricSystem.AU else const.hbar

    @property
    def k_e(self):
        return (
            1.0 if self.ms == MetricSystem.AU else 1.0 / 4 / const.pi / const.epsilon_0
        )

    @property
    def a_0(self):
        return self.hbar**2 / self.m_e / self.e**2 / self.k_e
