import numpy as np


class Numerov:
    def step(
        self,
        init_i: tuple[float, float],
        init_o: tuple[float, float],
        k: np.ndarray,
        h: float,
        m_idx: int,
    ):
        """
        Inwards integration till the matching point x_m.
        Initial guess asymptotics: u(r) ~ r->inf
        """
        k_i = k[m_idx:]
        v_i = np.flip(self.compute(h, np.flip(k_i), init_i))

        """
        Outwards integration till the matching point x_m.
        Initial guess asymptotics: u(r) ~ r->0.
        """
        k_o = k[0 : m_idx + 1]
        v_o = self.compute(h, k_o, init_o)

        """
        Connect solutions.
        """
        v_i *= v_o[-1] / v_i[0]
        v_n_l = np.concatenate((v_o[:-1], v_i))  # exclude redundant point

        return v_n_l

    def compute(
        self,
        h: float,
        k: np.ndarray,
        init_values: tuple[float, float] = (0, 0.001),
    ) -> np.ndarray:
        size = k.shape[0]

        psi_0, psi_1 = init_values
        psi = np.zeros(size)
        psi[0] = psi_0
        psi[1] = psi_1

        for i in range(2, size):
            """
            Stencil geometry:
               iter dir -->
            [..., *, *, *, ...]
                  ^  ^  ^
                  |  |  |
                 i2  i1 i
            """

            c_i2 = 1 + k[i - 2] * h**2 / 12
            psi_i2 = psi[i - 2]

            c_i1 = 2 - 5 * k[i - 1] * h**2 / 6
            psi_i1 = psi[i - 1]

            c_i = 1 + k[i] * h**2 / 12
            psi_i = (c_i1 * psi_i1 - c_i2 * psi_i2) / c_i

            psi[i] = psi_i

        return psi

    def get_k_correction(
        self, k: np.ndarray, psi: np.ndarray, m_idx: int, h: float
    ) -> float:
        psi_ml, psi_m, psi_mr = psi[m_idx - 1], psi[m_idx], psi[m_idx + 1]
        k_ml, k_m, k_mr = k[m_idx - 1], k[m_idx], k[m_idx + 1]

        c_ml = 1 + k_ml * h**2 / 12
        c_mr = 1 + k_mr * h**2 / 12

        k_m_upd = (2 - c_ml * psi_ml / psi_m - c_mr * psi_mr / psi_m) * 6 / 5 / h**2
        return k_m_upd - k_m
