import math
import numpy as np
import matplotlib.pyplot as plt
import ce.h_atom as h_a
import ce.grid as grid

from ce.utils import *
from ce.scheme import Numerov
from ce.wave_function import WaveFunction
from ce.enums import MetricSystem
from ce.potential import CoulombPotential
from ce.unit_provider import UnitProvider

BOUNDARY_FACTORS = (1.0 / 100, 100)
PLOT_REF = True
MAX_ITER = 50


def config_plt(grid_num, title, xlabel, ylabel):
    plt.subplot(grid_num)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)


def h_numerov(
    q_num: tuple[int, int], metric: MetricSystem = MetricSystem.AU, E_0: float = -1.0
) -> tuple[WaveFunction, grid.Grid1DLog, float]:
    up = UnitProvider(metric)
    a_0 = up.a_0

    """
    Grid initialization
    """
    A = math.log(a_0 * BOUNDARY_FACTORS[0])
    B = math.log(a_0 * BOUNDARY_FACTORS[1])
    nodes = 500
    g = grid.Grid1DLog(A, B, nodes)
    h = g.step

    """
    Energy value initialization
    """
    potential = CoulombPotential(up)
    V = potential.eval(g.r)
    n, l = q_num
    E = E_0

    """
    Define asymptotics based on
    H-atom results
    """
    node_factor = -1 if (n + l) % 2 == 0 else 1
    init_i = (
        lambda E, r: node_factor * math.exp(-math.sqrt(2 * abs(E)) * r) / math.sqrt(r)
    )
    init_o = lambda r: r ** (l + 1.0 / 2)

    numerov = Numerov()
    v_nl = np.zeros(nodes)
    v_nl_norm = 1.0

    """
    Matching point computation.
    Use Classical turning point as a matching point.
    """
    CTP = math.log(potential.get_r(E))
    m_idx = g.get_nearest(CTP)
    if m_idx > nodes - 3:
        m_idx = nodes - 3
    if m_idx < 3:
        m_idx = 3

    """
    k^2 from Schrodinger Equation.
    Defined for Numerov integration.
    """
    k = 2 * (E - V) * g.r**2 - (l + 0.5) ** 2

    """
    Initial guess values based on asymptotic
    results.
    """
    psi_i0, psi_i1 = (
        init_i(E, g.r[-1]),
        init_i(E, g.r[-2]),
    )
    psi_o0, psi_o1 = init_o(g.r[0]), init_o(g.r[1])

    """
    Numerov sweep to obtain the transformed
    wave function v = u / sqrt(r)
    """
    v_nl = numerov.step(
        (psi_i0, psi_i1),
        (psi_o0, psi_o1),
        k,
        h,
        m_idx,
    )
    v_nl_norm = norm_log(h, g.r, v_nl)
    v_nl /= v_nl_norm

    """
    Eigenvalue correction computation
    """
    dk = numerov.get_k_correction(k, v_nl, m_idx, h)
    dE = dk * h * v_nl[m_idx] ** 2 / 2

    u_nl = v_nl * np.sqrt(g.r)
    wf = WaveFunction(u_nl / g.r, g.r, E)
    return wf, g, dE


def find_eigen_energy(
    q_num: tuple[int, int], metric: MetricSystem = MetricSystem.AU, E_0: float = -1.0
):
    """
    This routine looks for any eigenvalue.
    Obtained result does not neccessarily satisfy
    the desired orbital energy. See `find_h_eigen_energy`.
    """

    wf = None
    n_iter = 0
    while True:
        wf, _, dE = h_numerov(q_num, metric, E_0)
        E_0 += dE
        n_iter += 1

        """
        Search stops if:
        i) Convergence criterion is met
        ii) Maximum number of iterations is exceeded
        """
        if dE <= 1e-12 or n_iter >= MAX_ITER:
            break

    assert wf is not None
    return wf, E_0


def find_h_eigen_energy(
    q_num: tuple[int, int], metric: MetricSystem = MetricSystem.AU, E_0: float = -1.0
):
    """
    Searches for a concrete energy level
    based on the nodes WF should have.
    """
    n, l = q_num
    desired_nodes = n - l - 1
    n_correction = 0
    wf = None

    while True:
        wf, E_0 = find_eigen_energy(q_num, metric, E_0)
        nodes = wf.nodes
        n_correction += 1

        if nodes == desired_nodes or n_correction >= MAX_ITER:
            break

        if nodes < desired_nodes:
            """
            If too little nodes we are too close to the GS
            """
            E_0 /= 2
        else:
            """
            If too many nodes we are too far from the GS
            """
            E_0 *= 2

    assert wf is not None
    return wf, E_0, n_correction


if __name__ == "__main__":
    metric = MetricSystem.AU
    up = UnitProvider(metric)
    a_0 = up.a_0

    q_nums = [(1, 0), (4, 3), (6, 2)]
    print("|  (n, l)  |    E   |   E_0  | N_iter")
    for q_num in q_nums:
        purturb = 0.97
        E_0 = h_a.energy(q_num[0], up)
        wf, E, n_correction = find_h_eigen_energy(q_num, metric, purturb * E_0)
        print(f"|  {q_num}  | {E:.3f} | {E_0:.3f} | {n_correction}")
