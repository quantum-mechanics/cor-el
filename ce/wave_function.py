import numpy as np


class WaveFunction:
    def __init__(
        self,
        val: np.ndarray,
        r: np.ndarray,
        e: float,
        n: int = None,
        l: int = None,
        m: int = None,
        s: int = None,
    ):
        self.val = val
        self.r = r
        self.e = e

    @property
    def density(self):
        return np.conjugate(self.val) * self.val

    @property
    def nodes(self):
        psi = self.val
        return np.where((psi[:-1] * psi[1:]) < 0)[0].shape[0]
