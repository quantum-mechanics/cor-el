import numpy as np

from ce.unit_provider import UnitProvider


class CoulombPotential:
    def __init__(self, up: UnitProvider):
        self.up = up

    def eval(self, x: np.ndarray) -> np.ndarray:
        """
        Compute potential energy vector v(x)
        """
        up = self.up
        return -up.e * up.e * up.k_e / x

    def get_r(self, energy: float) -> float:
        """
        Compute x at which the value of energy
        is the provided energy
        """
        up = self.up
        return -up.e * up.e * up.k_e / energy
