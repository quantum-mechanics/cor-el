import abc
import numpy as np


class Grid:
    @abc.abstractmethod
    def generate(self):
        pass


class Grid1D(Grid):
    """
    It is a 1D uniformly spaced grid, which can also
    be used for generating radial domain.

    Attributes
    ----------

    a: float - lower domain boundary
    b: float - upper domain boundary
    n: int - number of intervals


    r: np.ndarray - vector of nodes (discrete domain)
    """

    def __init__(self, a=0.0, b=1.0, n=100):
        self.a = a
        self.b = b
        self.n = n
        self.num_nodes = n + 1

        self.generate()

    def generate(self):
        self.r = np.linspace(self.a, self.b, self.num_nodes)

    @property
    def step(self) -> float:
        return self.r[1] - self.r[0]

    def __str__(self):
        return "Uniform 1D"


class Grid1DLog(Grid1D):
    """
    The domain of the logspace is not equivalent
    to the physical domain, but is related to it
    via the following equation:

    r(x) = e^x,
    where d is the point from the the row domain [a, b] -> [e^a, e^b]

    Attributes
    ----------

    --||-- inherited from Grid1D

    x: np.ndarray - vector of auxilary grid, which is is used to
        define physical radius.
    """

    def get_nearest(self, point: float) -> int:
        diffs = np.abs(self.x - point)
        return np.argmin(diffs)  # pyright: ignore

    def generate(self):
        self.x = np.linspace(self.a, self.b, self.num_nodes)
        self.r = np.logspace(self.a, self.b, self.num_nodes, base=np.e)

    @property
    def step(self) -> float:
        return self.x[1] - self.x[0]

    def __str__(self):
        return "Logarothmic 1D"
